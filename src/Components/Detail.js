import React, {Component} from 'react';

import logoDefault from '../Images/default-banner-business.jpg';
import '../Sass/Enterprise.scss';

export default class Detail extends Component{

    constructor(props){
        super(props)
        this.redirectHome = this.redirectHome.bind(this);
        this.state = {
            titleReference: 'Título da Empresa',
            imagemReference: logoDefault,
            descriptionReference: 'Descrição da empresa...',
            redirect: '/home',
        }
    }

    redirectHome(){
        window.history.back();
    }
    componentDidMount(){
        fetch("https://empresas.ioasys.com.br/api/v1/enterprises/"+sessionStorage.detailReference,{
            method: 'GET',
            headers: new Headers({
                "Content-type": "application/json;",
                'access-token': sessionStorage.credentials0,
                'client': sessionStorage.credentials1,
                'uid': sessionStorage.credentials2,
            })
        })
        .then(res => res.json())
        .then(json => {
            this.setState({
                titleReference: json.enterprise.enterprise_name,
                descriptionReference: json.enterprise.description,
            })


        })
    }

    render(){
        return(
            <>
                <header className="header title-detail">
                    <button className="header__back" onClick={this.redirectHome}></button>
                    <h1>{this.state.titleReference}</h1>
                </header>
                <section className="container-navigation">
                    <article className="card-detail">
                        <figure>
                            <img src={this.state.imagemReference} alt=""/>
                        </figure>
                        <p>{this.state.descriptionReference}</p>
                    </article>
                </section>
            </>
        )
    }
}