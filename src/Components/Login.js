import React, { Component } from 'react';
import { Redirect } from "react-router-dom";

import '../Sass/Login.scss';
import logoIoasys from '../Images/logo-home.png';
import iVPShow from '../Images/visibility.svg';
import iVPHide from '../Images/invisible.svg';




export default class Login extends Component{
    
    constructor(props){
        super(props)
        this.validaCredentials = this.validaCredentials.bind(this);
        this.requestEmail = this.requestEmail.bind(this);
        this.requestPassword = this.requestPassword.bind(this);
        this.hideShowPass = this.hideShowPass.bind(this);
        this.resetInput = this.resetInput.bind(this);

        this.state = {
            vEmail: '',
            vPassword: '',
            mInputInvalid: '',
            iVisPas: false,
            typePassword: false,
            redirect: null,
            _aToken: '',
            _aClient: '',
            _aUID: ''
        }

        this.email = React.createRef();
    }

    componentDidMount(){ 
        this.email.current.focus() ;
        this.elementsFormLogin = document.querySelectorAll('.container-login__input input');
        
    }

    hideShowPass(){ 
        this.setState({ 
            typePassword: !this.state.typePassword,
            iVisPas: !this.state.iVisPas,
        }) 
    }

    requestEmail(e){ 
        this.setState({ 
            vEmail: e.target.value 
        }) 
    }

    requestPassword(e){ 
        this.setState({ 
            vPassword: e.target.value 
        }) 
    }

    resetInput(e){
        if(e.target.parentNode.classList.contains('invalid-input')){
            e.target.parentNode.classList.remove('invalid-input')
            this.setState({
                mInputInvalid: '',
            })
        }
    }

    validaCredentials(e){
        
        e.preventDefault();
        let countI = 0;
        this.elementsFormLogin.forEach( (i, idx) => {
            if(this.elementsFormLogin[idx].value === ''){
                this.elementsFormLogin[idx].parentNode.classList.add('invalid-input')
                this.setState({
                    mInputInvalid: 'Credenciais informadas são inválidas, tente novamente.',
                })
            }else{ countI++ }
            
            if(countI === 2){
                //console.log(this.state.vEmail, this.state.vPassword);
                let login = {
                    email: this.state.vEmail,
                    password: this.state.vPassword,
                }

                fetch('https://empresas.ioasys.com.br/api/v1/users/auth/sign_in', {
                    method: 'POST',
                    headers: {"Content-type": "application/json;"},
                    body: JSON.stringify(login),
                })
                .then(res => {

                    if(res.status!==200){
                        this.setState({
                            mInputInvalid: 'Credenciais informadas são inválidas, tente novamente.',
                        })
                    }else{
                        this.setState({
                            _aToken: res.headers.get('access-token'),
                            _aClient: res.headers.get('client'),
                            _aUID: res.headers.get('uid'),
                            redirect: '/home'
                        })
    
                        let dataSession = [ this.state._aToken, this.state._aClient, this.state._aUID ]
    
                        dataSession.forEach((elem, idx) => {
                            sessionStorage.setItem(`credentials${idx}`, elem);
                        })
                    }

                    
                })
                .catch(err => console.log(err))
            }
            
        })

    }

    render(){

        if (this.state.redirect) {
            return <Redirect to={this.state.redirect} />
        }

        return(
            <div className="container-login">
                <div className="container-login__area">
                    <figure className="logo">
                        <img src={logoIoasys} alt=""/>
                    </figure>
                    <h1 className="container-login__title">BEM-VINDO AO EMPRESAS</h1>
                    <p className="container-login__description">Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
                    <form>
                        <div className="mb-2 container-login__input">
                            <input className="ico-mail" placeholder="E-mail" type="mail" ref={this.email} onChange={this.requestEmail} onFocus={this.resetInput} />
                        </div>

                        <div className="mb-1 container-login__input">
                            <input className="ico-padlock" placeholder="Senha" type={(!this.state.typePassword) ? 'password' : 'text'} onChange={this.requestPassword} onFocus={this.resetInput}/>
                            <b onClick={this.hideShowPass} className="ivp">
                                <img src={(this.state.iVisPas) ? iVPShow : iVPHide } alt="Hide | Show" title="Hide | Show"/>
                            </b>
                        </div>

                        <p className="mb-1 msg msg-error">{this.state.mInputInvalid}</p>
                        <button className="btn btn-primary" type="submit" onClick={this.validaCredentials}>Entrar</button>
                    </form>
    
                </div>
            </div>
        )
    }
    
}