import React, { Component } from "react";
import '../Sass/Search.scss';

export default class Search extends Component{
    constructor(props){
        super(props)
        this.actionSearch = this.actionSearch.bind(this);
        this.smartSearch = this.smartSearch.bind(this);

    }

    actionSearch(){
        let searchArea = document.querySelector('.search-area');
        searchArea.classList.add('expande-search')
    }

    smartSearch(e){
        console.log(e.target.value)

        fetch("https://empresas.ioasys.com.br/api/v1/enterprises?name="+e.target.value,{
            method: 'GET',
            headers: new Headers({
                "Content-type": "application/json;",
                'access-token': sessionStorage.credentials0,
                'client': sessionStorage.credentials1,
                'uid': sessionStorage.credentials2,
            })
        })
        .then(res => res.json())
        .then(json => console.log(json))


    }

    render(){
        return(
            <div className="search-area">
                <input className="search-area__search" type="search" onChange={this.smartSearch} placeholder="Pesquisar" />
                <button className="search-area__icon" type="button" onClick={this.actionSearch} ></button>
            </div>
        )
        
    }
}