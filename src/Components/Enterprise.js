import React, {Component} from 'react';
import { Link } from "react-router-dom";
import '../Sass/Enterprise.scss';

export default class Enterprise extends Component{

    constructor(props){
        super(props)
        this.redirectDetail = this.redirectDetail.bind(this);
    }

    redirectDetail(){
        sessionStorage.setItem('detailReference', this.props.id );
    }

    render(){
        return(
            <Link className="link-enterprise" to="/Detail" onClick={this.redirectDetail}>
                <article>
                    <figure>
                        <img src={this.props.logo} alt="" />
                    </figure>
                    <div>
                        <input type="hidden" defaultValue={this.props.id} />
                        <h1>{this.props.enterpriseName}</h1>
                        <h2>{this.props.enterpriseDescription}</h2>
                        <h3>{this.props.enterpriseCountry}</h3>
                    </div>

                </article>
            </Link>
        )
    }
}