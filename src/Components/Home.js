import React, { Component } from 'react';

import logoIoasys from '../Images/logo-nav.png';
import logoDefault from '../Images/img-default.png';
import '../Sass/Home.scss';
import Enterprise from './Enterprise';

export default class Home extends Component{
    constructor(props){
        super(props)
        this.actionSearch = this.actionSearch.bind(this);
        this.smartSearch = this.smartSearch.bind(this);

        this.state = {
            enterprise: []
        }
        this.arrContent = [];
    }

    actionSearch(){
        let searchArea = document.querySelector('.search-area');
        searchArea.classList.add('expande-search')
    }

    smartSearch(e){
        this.arrContent = [];
        let resTyped = e.target.value;
        fetch("https://empresas.ioasys.com.br/api/v1/enterprises?name="+resTyped,{
            method: 'GET',
            headers: new Headers({
                "Content-type": "application/json;",
                'access-token': sessionStorage.credentials0,
                'client': sessionStorage.credentials1,
                'uid': sessionStorage.credentials2,
            })
        })
        .then(res => res.json())
        .then(json => {
            
            json.enterprises.forEach((i, idx) => {
                let enterpriseContent = <Enterprise 
                            key={Math.random().toFixed(6)}
                            logo={logoDefault} 
                            id={i.id}
                            enterpriseName={i.enterprise_name} 
                            enterpriseDescription={i.enterprise_type.enterprise_type_name}
                            enterpriseCountry={i.city}/>

                this.setState({
                    enterprise: this.arrContent.push(enterpriseContent),
                })

            });

        })

    }


    render(){

        let msgWait = <p className="container-navigation__intro">Clique na busca para iniciar.</p>
        let resSearch = (this.arrContent.length === 0) ? msgWait : this.arrContent

        return(
            <>
                <header className="header">
                    <img className="header__logo" src={logoIoasys} alt="" />
                    <div className="search-area">
                        <input className="search-area__search" type="search" onChange={this.smartSearch} placeholder="Pesquisar" />
                        <button className="search-area__icon" type="button" onClick={this.actionSearch} ></button>
                    </div>
                </header>
                <section className="container-navigation">
                    {resSearch}
                </section>
            </>
        )
    }
}